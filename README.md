# Resize Trello Task
    
    2023 Sep @ Fernando Dubal

Expand the Trello task editing window by up to 80%


## Installation

1. Download from: 

        https://gitlab.com/ferduby/resizetrellotask/-/archive/master/resizetrellotask-master.zip  
2. Descompres the file into the folder  
3. Open a chrome Tab chrome://extensions and turn On Modo Desarrollador  
4. Load unpacked and select the folder  

